import { AircraftModel, FlightModel } from "../schema";
import ProgressBar from "./ProgressBar";

interface RotationProps {
  currentAircraft: AircraftModel;
  currentRotation: FlightModel[];
  removeLastFlight: () => void;
  resetCurrentRotation: () => void;
}

function Rotation({
  currentAircraft,
  currentRotation,
  resetCurrentRotation,
  removeLastFlight,
}: RotationProps) {
  return (
    <div className="flex flex-col h-full">
      <div className="flex flex-row w-full pt-4 pb-2">
        <p className="text-2xl text-left flex-auto mb-2">
          Rotation {currentAircraft.ident}
        </p>
        {currentRotation.length > 0 && (
          <>
            <button
              type="button"
              className="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-xs px-5 py-2.5 mr-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
              onClick={resetCurrentRotation}
            >
              Reset rotation
            </button>
            <button
              type="button"
              className="active:outline-none focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-xs px-5 py-2.5 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
              onClick={removeLastFlight}
            >
              Remove last flight
            </button>
          </>
        )}
      </div>
      <div className="min-w-[300px] flex-auto flex flex-col w-full gap-4 overflow-y-scroll">
        {currentRotation.map((flight) => {
          return (
            <div
              className="rounded-xl border border-gray-200 bg-white p-4 shadow-md shadow-gray-100"
              key={flight.ident}
            >
              <p className="mb-4">Flight: {flight.ident}</p>
              <div className="flex justify-between items-center">
                <div className="text-left">
                  <p>{flight.origin}</p>
                  <p>{flight.readable_departure}</p>
                </div>
                <div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M13.5 4.5L21 12m0 0l-7.5 7.5M21 12H3"
                    />
                  </svg>
                </div>
                <div className="text-right">
                  <p>{flight.destination}</p>
                  <p>{flight.readable_arrival}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div className="flex-initial pt-2 mt-6 border-t-2 mb-2">
        <ProgressBar currentRotation={currentRotation} />
      </div>
    </div>
  );
}

export default Rotation;
