import { AircraftModel } from "../schema";

interface AircraftsProps {
  aircrafts?: AircraftModel[];
  handleOnClick: (aircraft: AircraftModel) => void;
  currentAircraft?: AircraftModel;
  currentRotationPercentUsage: number;
}

function Aircrafts({
  aircrafts,
  handleOnClick,
  currentAircraft,
  currentRotationPercentUsage,
}: AircraftsProps) {
  return (
    <div className="flex h-full flex-col">
      <h2 className="text-center text-2xl my-4">Aircrafts</h2>
      <div className="w-60 flex flex-col flex-auto rounded-xl border border-gray-200 bg-white p-2 shadow-md shadow-gray-100 overflow-y-scroll">
        {aircrafts ? (
          aircrafts.map((aircraft) => {
            const isActive = aircraft.ident === currentAircraft?.ident;
            return (
              <button
                className={`font-light text-gray-600 rounded-md p-2 hover:bg-green-100 ${
                  isActive ? "bg-green-100" : ""
                }`}
                onClick={() => handleOnClick(aircraft)}
                key={aircraft.ident}
              >
                <p className="text-lg text-center justify-between ">
                  {aircraft.ident}
                </p>
                {isActive && <p>{currentRotationPercentUsage}%</p>}
              </button>
            );
          })
        ) : (
          <p className="text-lg text-center justify-between font-light text-gray-600">
            No aircrafts available
          </p>
        )}
      </div>
    </div>
  );
}

export default Aircrafts;
