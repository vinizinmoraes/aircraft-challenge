import { FlightModel } from "../schema";

interface FlightsProps {
  flights?: FlightModel[];
  handleOnClick: (flight: FlightModel) => void;
  currentRotation: FlightModel[];
}

function Flights({ flights, handleOnClick, currentRotation }: FlightsProps) {
  let availableFlights = flights;

  if (currentRotation.length > 0) {
    availableFlights = flights?.filter((flight) => {
      const currentLastFlight = currentRotation[currentRotation.length - 1];
      const timeoutBetweenFlights = 20 * 60;
      return (
        currentLastFlight.destination === flight.origin &&
        currentLastFlight.arrivaltime + timeoutBetweenFlights <=
          flight.departuretime
      );
    });
  }

  return (
    <div className="flex h-full flex-col">
      <h2 className="text-center text-2xl my-4">Flights</h2>
      <div className="w-60 flex flex-col flex-auto rounded-xl border border-gray-200 bg-white p-2 shadow-md shadow-gray-100 overflow-y-scroll">
        {availableFlights ? (
          availableFlights.map((flight) => {
            return (
              <button
                className="rounded-md p-2 hover:bg-green-100 text-center justify-between font-light text-gray-600"
                onClick={() => handleOnClick(flight)}
                key={flight.ident}
              >
                <p className="mb-2">{flight.ident}</p>
                <div className="flex justify-between items-center">
                  <div className="text-left">
                    <p>{flight.origin}</p>
                    <p>{flight.readable_departure}</p>
                  </div>
                  <div className="text-right">
                    <p>{flight.destination}</p>
                    <p>{flight.readable_arrival}</p>
                  </div>
                </div>
              </button>
            );
          })
        ) : (
          <p className="text-lg text-center justify-between font-light text-gray-600">
            No flights available
          </p>
        )}
      </div>
    </div>
  );
}

export default Flights;
