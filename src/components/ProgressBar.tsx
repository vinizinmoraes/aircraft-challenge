import { dayInSeconds, turnaroundPercent } from "../constants";
import { FlightModel } from "../schema";

interface ProgressBarProps {
  currentRotation: FlightModel[];
}

function ProgressBar({ currentRotation }: ProgressBarProps) {
  return (
    <div className="flex flex-col">
      <div className="w-full flex justify-between mb-1">
        <div className="relative pb-3">
          00:00
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-4 h-4 absolute bottom-0 left-0 -ml-2"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 4.5v15m0 0l6.75-6.75M12 19.5l-6.75-6.75"
            />
          </svg>
        </div>
        <div className="relative pb-3">
          12:00
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-4 h-4 absolute bottom-0 left-1/2 -ml-2"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 4.5v15m0 0l6.75-6.75M12 19.5l-6.75-6.75"
            />
          </svg>
        </div>
        <div className="relative pb-3">
          23:59
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-4 h-4 absolute bottom-0 right-0 -mr-2"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 4.5v15m0 0l6.75-6.75M12 19.5l-6.75-6.75"
            />
          </svg>
        </div>
      </div>
      <div className="w-full bg-gray-400 leading-none h-5 relative">
        {currentRotation.map((flight) => {
          const leftPosition = (flight.departuretime / dayInSeconds) * 100;
          const size =
            ((flight.arrivaltime - flight.departuretime) / dayInSeconds) * 100;

          return (
            <div key={flight.ident}>
              <div
                className="bg-green-600 absolute text-xs font-medium text-blue-100 text-center h-full leading-none"
                style={{
                  width: `${size}%`,
                  left: `${leftPosition}%`,
                }}
              />
              <div
                className="bg-purple-600 absolute text-xs font-medium text-blue-100 text-center h-full leading-none"
                style={{
                  width: `${turnaroundPercent}%`,
                  left: `${leftPosition + size}%`,
                }}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default ProgressBar;
