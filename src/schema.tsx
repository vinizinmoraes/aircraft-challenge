import { z } from "zod";

export const AircraftModelSchema = z.object({
  ident: z.string(),
  type: z.string(),
  economySeats: z.number(),
  base: z.string(),
});

export type AircraftModel = z.infer<typeof AircraftModelSchema>;

export const FlightModelSchema = z.object({
  ident: z.string(),
  departuretime: z.number(),
  arrivaltime: z.number(),
  readable_departure: z.string(),
  readable_arrival: z.string(),
  origin: z.string(),
  destination: z.string(),
});

export type FlightModel = z.infer<typeof FlightModelSchema>;
