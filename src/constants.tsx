export const dayInSeconds = 60 * 60 * 24;
export const turnaroundPercent = ((20 * 60) / dayInSeconds) * 100;
