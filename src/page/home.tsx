import { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import Aircrafts from "../components/Aircrafts";
import { fetchAircrafts, fetchFlights } from "../api";
import Loading from "../components/Loading";
import { AircraftModel, FlightModel } from "../schema";
import Flights from "../components/Flights";
import Rotation from "../components/Rotation";
import { dayInSeconds, turnaroundPercent } from "../constants";

function Home() {
  const [currentAircraft, setCurrentAircraft] = useState<AircraftModel>();
  const [currentRotation, setCurrentRotation] = useState<FlightModel[]>([]);

  const aircraftsQuery = useQuery({
    queryKey: ["aircrafts"],
    queryFn: fetchAircrafts,
  });

  const flightsQuery = useQuery({
    queryKey: ["flights"],
    queryFn: fetchFlights,
  });

  const handleAircraftClick = (aircraft: AircraftModel) => {
    setCurrentAircraft(aircraft);
    setCurrentRotation([]);
  };

  const handleFlightClick = (flight: FlightModel) => {
    setCurrentRotation((prevRotation) => [...prevRotation, flight]);
  };

  const removeLastFlight = () => {
    setCurrentRotation((prevRotation) => [...prevRotation.slice(0, -1)]);
  };

  const resetCurrentRotation = () => {
    setCurrentRotation([]);
  };

  const currentRotationPercentUsage = currentRotation.reduce((acc, flight) => {
    const flightTimePercent =
      ((flight.arrivaltime - flight.departuretime) / dayInSeconds) * 100;

    return Math.floor(acc + flightTimePercent + turnaroundPercent);
  }, 0);

  if (aircraftsQuery.isLoading || flightsQuery.isLoading) return <Loading />;

  if (aircraftsQuery.error || flightsQuery.error)
    return <div>An error has occurred</div>;

  return (
    <div className="flex flex-row p-4 gap-4 h-screen">
      <div className="flex-initial">
        <Aircrafts
          aircrafts={aircraftsQuery.data}
          handleOnClick={handleAircraftClick}
          currentAircraft={currentAircraft}
          currentRotationPercentUsage={currentRotationPercentUsage}
        />
      </div>
      {currentAircraft ? (
        <>
          <div className="flex-auto">
            <Rotation
              currentRotation={currentRotation}
              currentAircraft={currentAircraft}
              removeLastFlight={removeLastFlight}
              resetCurrentRotation={resetCurrentRotation}
            />
          </div>
          <div className="flex-initial">
            <Flights
              handleOnClick={handleFlightClick}
              flights={flightsQuery.data}
              currentRotation={currentRotation}
            />
          </div>
        </>
      ) : (
        <div className="flex-auto pt-16 text-2xl">Select an aircraft</div>
      )}
    </div>
  );
}

export default Home;
