import { z } from "zod";
import { AircraftModel, AircraftModelSchema } from "./schema";

export const fetchAircrafts = async (): Promise<AircraftModel[]> => {
  const response = await fetch(
    "https://recruiting-assessment.alphasights.com/api/aircrafts"
  );
  const usersJson = await response.json();
  const users = z.array(AircraftModelSchema).parse(usersJson);
  return users;
};

export const fetchFlights = () => {
  return fetch(
    "https://recruiting-assessment.alphasights.com/api/flights"
  ).then((res) => res.json());
};
